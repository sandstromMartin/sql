import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {

        // Setup
        String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
        Connection conn = null;
        //array to store the values for the ID, first and last name from customer.
        ArrayList<Customer> customerList = new ArrayList<Customer>();

        try{
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT  CustomerId, FirstName, LastName FROM Customer");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {


                //store the resulst from the database into the array above.
                customerList.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName")
                        ));
            }
            System.out.println("Enter customer ID number or any other key to get a random ID number: ");
            Scanner scan = new Scanner(System.in);


            //create a random int value.
            int random = (int) (Math.random() * (customerList.size()));

            //as long as the scanner includes a int value, it will be stored in the inputID and then looped in the arraylist that stores the customers.
            // when the inputid matches a customernumberId in a cosutmer, the id, name and lastname will be printed.

            if (scan.hasNextInt()) {
                int inputId= Integer.parseInt(scan.nextLine());
                for (Customer c : customerList) {
                    if (c.getCustomerID()== inputId) {
                        System.out.println(c.getCustomerID() + ". " + c.getFirstName() + " " + c.getLastName());
                    }
                }
                //a new statement will be sent to the database and matches each primarykeys, from customer to invovice.....till genre. the count() as most will count the names in genre and group them.

                preparedStatement =
                        conn.prepareStatement("SELECT  Genre.Name, COUNT (Genre.Name) as Most FROM Customer, Invoice, InvoiceLine, Track, Genre WHERE Customer.CustomerId=Invoice.CustomerId AND Invoice.InvoiceId=InvoiceLine.InvoiceId AND InvoiceLine.TrackId=Track.TrackId AND Track.GenreId=Genre.GenreId AND customer.CustomerId = ? GROUP BY Genre.Name ORDER BY COUNT (Genre.Name) DESC LIMIT 1");

                preparedStatement.setInt(1, inputId);
            } else if (scan.hasNextLine() ){

                //diffrent from above is if the scanner not inclueds an int. a random number will then be created and represent the custumerIdnumber.
                for (Customer c : customerList) {

                    if (c.getCustomerID()== random) {
                        System.out.println(c.getCustomerID() + ". " + c.getFirstName() + " " + c.getLastName());
                    }
                }

                preparedStatement =
                        conn.prepareStatement("SELECT  Genre.Name, COUNT (Genre.Name) as Most FROM Customer, Invoice, InvoiceLine, Track, Genre WHERE Customer.CustomerId=Invoice.CustomerId AND Invoice.InvoiceId=InvoiceLine.InvoiceId AND InvoiceLine.TrackId=Track.TrackId AND Track.GenreId=Genre.GenreId AND customer.CustomerId = ? GROUP BY Genre.Name ORDER BY COUNT (Genre.Name) DESC LIMIT 1");

                preparedStatement.setInt(1, random);
            }
            resultSet= preparedStatement.executeQuery();

            ArrayList<Customer> customerGenreList = new ArrayList<Customer>();


            //a new array will be created and store the values of the results from the database. It will store the name string name of the genre.
           while (resultSet.next()){
               customerGenreList.add(new Customer(resultSet.getString("Name")));

           }
           //then print out the name of the genre that is stored in the list of genre.


          for (Customer c : customerGenreList){
               System.out.println(c.getNameOfGenre());

           }
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
    }
}
