public class Customer {
    private int CustomerId;
    private String FirstName;
    private String LastName;
    private String NameOfGenre;


    public Customer(int customerId, String firstName, String lastName) {
        this.CustomerId = customerId;
        this.FirstName = firstName;
        this.LastName=lastName;
    }
    public Customer (String nameOfGenre){
        this.NameOfGenre=nameOfGenre;
    }

    public String getNameOfGenre() {
        return NameOfGenre;
    }

    public void setNameOfGenre(String nameOfGenre) {
        NameOfGenre = nameOfGenre;
    }

    public int getCustomerID() {
        return CustomerId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
}
